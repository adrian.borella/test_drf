from django.db import models


class TaskManager(models.Manager):
        
    def get_done(self):
        return super().get_queryset().filter(is_done=True)

    def get_pending(self):
        return super().get_queryset().filter(is_done=False)