from django.db import models
from .manager import TaskManager


class Task(models.Model):
    title = models.CharField('Title', max_length=100)
    description = models.TextField('Description')
    is_done = models.BooleanField()

    objects = TaskManager()

    def __str__(self):
        return f'{self.title}:{self.is_done}'
    