from django.test import TestCase
from .models import Task

from rest_framework.test import APIClient, APIRequestFactory
from rest_framework import status

from .views import TaskViewSet

factory = APIRequestFactory()

class TestTaskManager(TestCase):
    fixtures = ["task.json"]

    def test_is_done(self):
        expected = 1
        result = Task.objects.get_done()
        self.assertEqual(expected, len(result))
    
    def test_is_pending(self):
        expected = 2
        result = Task.objects.get_pending()
        self.assertEqual(expected, len(result))

    def test_get(self):
        expected = 3
        request = factory.get('/api/tasks', format='json')
        view = TaskViewSet.as_view({'get':'list'})
        result = view(request)
        
        self.assertEqual(status.HTTP_200_OK, result.status_code)
        self.assertEqual(expected, len(result.data))
    
    def test_post(self):
        expected = 3
        data = {
            'title' : 'Test Post',
            'description':'Test description',
            'is_done': False
        }
        request = factory.post('/api/tasks',data=data, format='json')
        view = TaskViewSet.as_view({'get':'list'})
        result = view(request)
        
        self.assertEqual(status.HTTP_200_OK, result.status_code)
        import ipdb; ipdb.set_trace()
        self.assertEqual(data['is_done'], result.data.is_done)
