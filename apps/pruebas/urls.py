from rest_framework import routers
from .views import TaskViewSet
from django.conf.urls import url, include

router = routers.DefaultRouter()
router.register('tasks', TaskViewSet)

urlpatterns = [
    url('', include(router.urls))
]
